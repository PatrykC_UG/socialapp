using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TwitterMvc.Models;
using TwitterMvc.Services.Interfaces;

namespace TwitterMvc.Controllers
{
    public class QuestionAndAnswerController : Controller
    {
        private readonly IQuestionAndAnswerService _qnaService;
        private readonly UserManager<CustomUser> _userManager;

        public QuestionAndAnswerController(IQuestionAndAnswerService qnaService, UserManager<CustomUser> userManager)
        {
            _qnaService = qnaService;
            _userManager = userManager;
        }
        
        [HttpPost]
        public async Task<IActionResult> SendQuestion(string receiverId, string message)
        {
            var sender = await _userManager.GetUserAsync(User);
            var result = await _qnaService.SendQuestion(sender.Id, receiverId, message);
            
            return RedirectToAction("Index", "Profile", new { userId = receiverId });
        }

        [HttpGet]
        public async Task<IActionResult> Questions()
        {
            var user = await _userManager.GetUserAsync(User);
            var result = await _qnaService.GetQuestions(user.Id);

            if (result.Content.Count == 0) ViewBag.NoQuestionMessage = "You don't have any questions :(";
            
            return View(result.Content);
        }

        [HttpPost]
        public async Task<IActionResult> AnswerToQuestion(int questionId, string message)
        {
            var user = await _userManager.GetUserAsync(User);
            var result = await _qnaService.AnswerToQuestion(user.Id, questionId, message);
            
            return RedirectToAction("Questions");
        }
    }
}